export default {
  message: {
    title: "Welcome to Your Vue.js App",
    guide: "For a guide and recipes on how to configure / customize this project,",
    checkout: "check out the",
    plugins: "Installed CLI Plugins",
    links: "Essential Links",
    ecosystem: "Ecosystem",
  },
};
