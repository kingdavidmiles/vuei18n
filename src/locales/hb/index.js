export default {
  message: {
    title: "ברוך הבא לאפליקציית Vue.js שלך",
    guide: "לקבלת מדריך ומתכונים כיצד להגדיר / להתאים אישית את הפרויקט הזה,",
    checkout: "לבדוק את",
    plugins: "תוספי CLI מותקנים",
    name: "דוד",
  },
};
