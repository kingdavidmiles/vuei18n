import Vue from "vue";
import VueI18n from "vue-i18n";
import en from "./en/index";
import hb from "./hb/index";

Vue.use(VueI18n);

const messages = {
  en: en,

  hb: hb,
};

const i18n = new VueI18n({
  locale: localStorage.getItem("lang"), // set locale
  fallbackLocale: "hb", // set fallback locale
  messages, // set locale messages
});

export default i18n;
